
// REQUIREMENTS
var fs = require('fs');
var rimraf = require('rimraf');
var yaml = require('js-yaml');

var testTempDirectory = './src/test/temp/temp';
var testDirectory = './src/test/temp/';
var testSwaggerFile = './src/test/resources/swagger.yml';
var testOverrideDirectory = './src/test/resources/overrides';

const { execute, clazzIsChildOfParent, getClassesFromSwaggerFile, prefixLines, replacingClazz, replacing, generateTemplate, generatePathsSection} = require('./../../main/js/crud-swagger-generation.js');

function cleanTestDirectory() {
    try {
       fs.unlinkSync(testDirectory);
    } catch(err) {
    }
    fs.mkdirSync(testTempDirectory, {recursive :true});
}


var goodConfig = {overridesDirectory: testOverrideDirectory, templatesDir: './templates/', clazzExceptions:[], outputDirectory: testDirectory, tempDirectory: testTempDirectory, swaggerFile: testSwaggerFile, apiVersion: '1.0.0'};
test("given_good_config_when_execute_then_return_filepath", () => {
    cleanTestDirectory();
    expect(execute(goodConfig)).toBe(testDirectory+'/index.yaml');
});

test("given_good_config_w/_overrides_files_when_execute_then_return_filepath", () => {
    cleanTestDirectory();
    expect(execute({overridesDirectory: testOverrideDirectory, clazzExceptions:[], templatesDir:'./templates/', outputDirectory: testDirectory, tempDirectory: testTempDirectory, swaggerFile: testSwaggerFile, apiVersion: '1.0.0'})).toBe(testDirectory+'/index.yaml');

    expect(fs.readFileSync(testTempDirectory+'/Component.path.uuid.yaml', 'utf8')).toBe(fs.readFileSync(testOverrideDirectory+'/Component.path.uuid.yaml', 'utf8'));
});

test("given_missing_config_when_execute_then_return_false", () => {
    expect(execute({})).toBe(false);
    expect(execute({templatesDir:'x', tempDirectory: 'y', outputDirectory: 'y', swaggerFile: 'y'})).toBe(false);
    expect(execute({templatesDir:'x', tempDirectory: 'y', outputDirectory: 'y', apiVersion: 'y'})).toBe(false);
    expect(execute({templatesDir:'x', tempDirectory: 'y', swaggerFile: 'y', apiVersion: 'y'})).toBe(false);
    expect(execute({apiVersion:'x', tempDirectory: 'y', outputDirectory: 'y', swaggerFile: 'y'})).toBe(false);
    expect(execute({apiVersion:'x', outputDirectory: 'y', swaggerFile: 'y', apiVersion:'y'})).toBe(false);
});

test("given_missing_template_when_execute_then_throw_exception", () => {
    cleanTestDirectory();
    try {
        execute({templatesDir:'./templates-not-exists/', outputDirectory: testDirectory, swaggerFile: testSwaggerFile, apiVersion: '1.0.0'});
        expect(true).toBe(false);
    } catch(error) {
        expect(error.code).toEqual('ENOENT');
    }
});
test("given_missing_swagger_file_when_execute_then_throw_exception", () => {
    cleanTestDirectory();
    try {
        execute({templatesDir:'./templates/', outputDirectory: testDirectory, swaggerFile: './test/resources/swagger-not-exists.yml', apiVersion: '1.0.0'});
        expect(true).toBe(false);
    } catch(error) {
        expect(error.code).toEqual('ENOENT');
    }
});

test("given_swaggerfile_when_getClassesFromSwaggerFile_then_return_classes", () => {
    var goodConfig = {swaggerFile:testSwaggerFile, clazzExceptions:[]};
    expect(getClassesFromSwaggerFile(goodConfig)).toEqual([ 'Component',
                                                                                'Asset',
                                                                                'Library',
                                                                                'Implementation',
                                                                                'ImplementationVersion',
                                                                                'ApiVersion',
                                                                                'LibraryApiVersion',
                                                                                'SourceRepository',
                                                                                'Pipeline',
                                                                                'Group',
                                                                                'User' ]);
});

test("given_swaggerfile_no_existing_when_getClassesFromSwaggerFile_then_throw_exception", () => {
    try {
        var badConfig = {swaggerFile:'./test/resources/swagger-no-exists.yml', clazzExceptions:[]};
        getClassesFromSwaggerFile(badConfig);
        expect(true).toBe(false);
    } catch(error) {
        expect(error.code).toEqual('ENOENT');
    }
});

test("given_PROGRAMMING_LANGUAGE_as_child_and_Resource_as_parent_when_clazzIsChildOfParent_then_return_false", () => {
    var document = yaml.safeLoad(fs.readFileSync(testSwaggerFile, 'utf8'));
    expect(clazzIsChildOfParent(document, 'PROGRAMMING_LANGUAGE', 'Resource')).toBe(false);;
});

test("given_User_as_child_and_Resource_as_parent_when_clazzIsChildOfParent_then_return_false", () => {
    var document = yaml.safeLoad(fs.readFileSync(testSwaggerFile, 'utf8'));
    expect(clazzIsChildOfParent(document, 'User', 'Resource')).toBe(true);
});


test("given_Library_as_child_and_Asset_and_Resource_as_parent_when_clazzIsChildOfParent_then_return_false", () => {
    var document = yaml.safeLoad(fs.readFileSync(testSwaggerFile, 'utf8'));
    expect(clazzIsChildOfParent(document, 'Library', 'Asset')).toBe(true);
    expect(clazzIsChildOfParent(document, 'Asset', 'Resource')).toBe(true);
    expect(clazzIsChildOfParent(document, 'Library', 'Resource')).toBe(true);
});


test("given_one_line_then_prefixLines_then_works", () => {
    expect(prefixLines('Y', 'X')).toBe('XY');
    expect(prefixLines('Y\nZ', 'X')).toBe('XY\nXZ');
});

test("given_key_value_then_replacing_then_works", () => {
    expect(replacing('Y{{Y}}Y{{Y}}YY', 'Y', 'Z')).toBe('YZYZYY');
});

test("given_classes_when_replacingClazz_then_replace", () => {
  expect(replacingClazz('{{clazz}} {{clazzPlural}} {{clazzLower}} {{clazzPluralLower}}', 'User')).toBe('User Users user users');
  expect(replacingClazz('{{clazz}} {{clazzPlural}} {{clazzLower}} {{clazzPluralLower}}', 'Repository')).toBe('Repository Repositories repository repositories');
});

test("given_classes_when_generateTemplate_then_return_correct_string", () => {
  expect(generateTemplate(['Component', 'Asset'], 'template {{clazz}}')).toBe('template Component\ntemplate Asset\n');
});

test("given_classes_when_generateTagsSection_then_return_correct_string", () => {
    cleanTestDirectory();
    expect(generatePathsSection(['Component', 'Asset'], 'template {{clazz}}', 'template2 {{clazz}}', goodConfig))
            .toBe(
            "\"/components\":\n    $ref: ./Component.path.yaml\n"
            +"\"/components/{resourceId}\":\n    $ref: ./Component.path.uuid.yaml\n"
            +"\"/assets\":\n    $ref: ./Asset.path.yaml\n"
            +"\"/assets/{resourceId}\":\n    $ref: ./Asset.path.uuid.yaml\n");
     expect(fs.readFileSync(testTempDirectory+'/Component.path.yaml', 'utf8')).toBe('template Component');
     expect(fs.readFileSync(testTempDirectory+'/Component.path.uuid.yaml', 'utf8')).toBe('template2 Component');
     expect(fs.readFileSync(testTempDirectory+'/Asset.path.yaml', 'utf8')).toBe('template Asset');
     expect(fs.readFileSync(testTempDirectory+'/Asset.path.uuid.yaml', 'utf8')).toBe('template2 Asset');
});
