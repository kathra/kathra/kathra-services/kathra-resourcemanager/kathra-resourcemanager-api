
const fs = require('fs');
const yaml = require('js-yaml');
const path = require('path');


// FUNCTIONS

/**
Execute generation
**/
var execute = function(config) {

    if (config.templatesDir == null) {
        console.error("config.templatesDir missing");
        return false;
    }
    if (config.outputDirectory == null) {
         console.error("config.outputDirectory missing");
        return false;
    }
    if (config.swaggerFile == null) {
         console.error("config.swaggerFile missing");
        return false;
    }
    if (config.apiVersion == null) {
         console.error("config.apiVersion missing");
        return false;
    }
    // TEMPLATES
    var indexTemplateData = fs.readFileSync(config.templatesDir + 'index.template.yaml', 'utf8');
    var definitionTemplateData = fs.readFileSync(config.templatesDir + 'definition.template.yaml', 'utf8');
    var tagTemplateData = fs.readFileSync(config.templatesDir + 'tag.template.yaml', 'utf8');
    var pathTemplateData = fs.readFileSync(config.templatesDir + 'path.template.yaml', 'utf8');
    var pathUuidTemplateData = fs.readFileSync(config.templatesDir + 'path.with.uuid.template.yaml', 'utf8');

    var classes = getClassesFromSwaggerFile(config);
    var indexToWrite = indexTemplateData;

    indexToWrite = replacing(indexToWrite,'apiVersion', config.apiVersion);
    indexToWrite = replacing(indexToWrite,'tags',  generateTemplate(classes, tagTemplateData));
    indexToWrite = replacing(indexToWrite,'definitions', generateTemplate(classes, definitionTemplateData));
    indexToWrite = replacing(indexToWrite,'paths', prefixLines(generatePathsSection(classes, pathTemplateData, pathUuidTemplateData, config), "  "));

    fs.writeFileSync(config.tempDirectory+'/index.yaml', indexToWrite, 'utf8');
    console.debug('Index file created : '+config.tempDirectory+'/index.yaml');
    if (config.overridesDirectory != null) {
        console.error("overridesDirectory is defined at '" + config.overridesDirectory + "'.. overriding files !");
        copyFolderRecursiveSync(config.overridesDirectory, config.tempDirectory);
    }
    return config.outputDirectory+'/index.yaml';
}

function copyFolderRecursiveSync( source, target ) {
    var files = [];

    //check if folder needs to be created or integrated
    var targetFolder = target;
    if ( !fs.existsSync( targetFolder ) ) {
        fs.mkdirSync( targetFolder );
    }

    //copy
    if ( fs.lstatSync( source ).isDirectory() ) {
        files = fs.readdirSync( source );
        files.forEach( function ( file ) {
            var curSource = path.join( source, file );
            if ( fs.lstatSync( curSource ).isDirectory() ) {
                copyFolderRecursiveSync( curSource, targetFolder );
            } else {
                copyFileSync( curSource, targetFolder );
            }
        } );
    }
}


function copyFileSync( source, target ) {
    console.debug("Copy file " + source + " into directory " + target);
    var targetFile = target;
    //if target is a directory a new file with the same name will be created
    if ( fs.existsSync( target ) ) {
        if ( fs.lstatSync( target ).isDirectory() ) {
            targetFile = path.join( target, path.basename( source ) );
        }
    }
    fs.writeFileSync(targetFile, fs.readFileSync(source));
}

/**
Generate tags section for swagger file from template and classes
*/
var generateTemplate  = function(classes, template) {
    var tagsToAdd = "";
    classes.forEach(function(clazz){
        tagsToAdd += replacingClazz(template, clazz)+"\n";
    });
    return tagsToAdd;
}
/**
Generate paths section for swagger file from templates and classes
*/
var generatePathsSection  = function(classes, template, templateUUID, config) {
    var extraPathsFile = config.overridesDirectory+'/extra-paths.yaml';
    var pathsToAdd = "";
    classes.forEach(function(clazz){
        var pluralNameClazz = getPluralNameForClazz(clazz);
        pathsToAdd += "\"/"+pluralNameClazz.toLowerCase()+"\":\n    $ref: ./" + clazz+".path.yaml\n";
        pathsToAdd += "\"/"+pluralNameClazz.toLowerCase()+"/{resourceId}\":\n    $ref: ./" + clazz+".path.uuid.yaml\n";
        fs.writeFileSync(config.tempDirectory+'/'+clazz+'.path.yaml', replacingClazz(template, clazz), 'utf8');
        console.debug('Path file created for class ' + clazz + ': '+config.tempDirectory+'/'+clazz+'.path.yaml');
        fs.writeFileSync(config.tempDirectory+'/'+clazz+'.path.uuid.yaml', replacingClazz(templateUUID, clazz), 'utf8');
        console.debug('Path file created for class ' + clazz + ': '+config.tempDirectory+'/'+clazz+'.path.uuid.yaml');
    });
    pathsToAdd += fs.existsSync(extraPathsFile) ? fs.readFileSync(extraPathsFile, 'utf8') : '';
    console.log(fs.readFileSync(extraPathsFile, 'utf8'));
    return pathsToAdd;
}


var replacing = function(input, key, value) {
    return input.replace(new RegExp('{{'+key+'}}', 'g'), value);
}

/**
Replace class's references

@input : text
@clazzName : class's name
@return : text modified
*/
var replacingClazz = function(input, clazzName) {
    var out = replacing(input, 'clazz', clazzName);
    var clazzNamePural = getPluralNameForClazz(clazzName);
    var out = replacing(out, 'clazzPlural', clazzNamePural);
    var out = replacing(out, 'clazzLower', clazzName.toLowerCase());
    var out = replacing(out, 'clazzPluralLower', clazzNamePural.toLowerCase());
    return out;
}
/**

@clazzName : class's name
@return : plural's class name
*/
var getPluralNameForClazz = function(clazzName) {
    return (clazzName.slice(-1) == "y") ? clazzName.replace(/y$/,"") + "ies" : clazzName + "s";
}

/**
Prefix each line
@input : text
@prefix : text to add at the beginning for each line
@return : text modifed
*/
var prefixLines = function(input, prefix) {
    return prefix+input.replace(new RegExp('\n', 'g'), "\n"+prefix);
}

/***
Extract class's name of resource from swagger file

@filepath : path of swagger file
@return : string[] containing class name
**/
var getClassesFromSwaggerFile = function(config) {
    var clazzChildOfParent = 'Resource';
    var filepath = config.swaggerFile;
    console.log('Extract class inherited from ' + clazzChildOfParent + ' included into file : ' + filepath);

    var document = yaml.safeLoad(fs.readFileSync(filepath, 'utf8'));

    var classes = [];
    for(var clazzName in document['definitions']){
        if (clazzName == clazzChildOfParent)
            continue;
        if (config.clazzExceptions.indexOf(clazzName) > -1)
            continue;
        var isChildOfParent = clazzIsChildOfParent(document, clazzName, clazzChildOfParent);
        console.debug("Class " + clazzName + " inherits form " + clazzChildOfParent + " : " + isChildOfParent);
        if (isChildOfParent)
            classes.push(clazzName);
    }

    console.log('Class found : ', classes);
    return classes;
}

/**
Check if a class inherits an parent class.

@document : swagger document as object
@clazzName : name's class checked
@parentClazzName : name's class parent
@return : boolean
**/
var clazzIsChildOfParent = function(document, clazzName, parentClazzName) {
    if (clazzName == parentClazzName)
        return true;
    if (document['definitions'][clazzName]['allOf'] == null)
        return false;
    var isResource = false;
    document['definitions'][clazzName]['allOf'].forEach(function(item) {
       if (item['$ref'] == null)
           return;
       var parentClazzNameOfChild = item['$ref'].replace('#/definitions/', '');
       if (clazzIsChildOfParent(document, parentClazzNameOfChild, parentClazzName)) {
            isResource = true;
       }
    });
    return isResource;
}


module.exports = { execute, clazzIsChildOfParent, getClassesFromSwaggerFile, prefixLines, replacingClazz, replacing, generateTemplate, generatePathsSection};