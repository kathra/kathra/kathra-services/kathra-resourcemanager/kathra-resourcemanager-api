/**
Script generating swagger file for crud operations from swagger-file containing model

@author Julien Boubechtoula
@version 1.0

*/

// CONFIG
const config = require('./config');
console.debug('config : ', config);

// REQUIREMENTS
var fs = require('fs');
var rimraf = require('rimraf');

var crudSwaggerGeneration = require('./src/main/js/crud-swagger-generation.js');


// clean directories
rimraf(config.tempDirectory, function(){
    // create directories
    fs.mkdirSync(config.tempDirectory, {recursive :true});
    // execute
    crudSwaggerGeneration.execute(config);
});


return 0;