const config = {
    apiVersion: '1.0.0-RC-SNAPSHOT',
    swaggerFile: './../specifications/Services/core/swagger.yaml',
    templatesDir: './templates/',
    outputDirectory: 'builds',
    tempDirectory: 'builds/temp',
    overridesDirectory: 'overrides',
    clazzExceptions: ['Resource','Asset']
};

module.exports = config;